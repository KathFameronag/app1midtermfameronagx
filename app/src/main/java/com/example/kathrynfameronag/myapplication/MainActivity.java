package com.example.kathrynfameronag.myapplication;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

        ImageView JCW , LJS;
        int ch = 2;


    public void animateImage(View view){
       // ImageView LJS = (ImageView) findViewById(R.id.LJS);
        //ImageView JCW = (ImageView) findViewById(R.id.JCW);

        if (ch%2 == 0) {
            LJS.animate()
                    .alpha(0f)
                    .rotationBy(3600)
                    .setDuration(2000);
            JCW.animate()
                    .alpha(1f)
                    .rotationBy(3600)
                    .setDuration(2000);
            ch++;
        }
        else {
            JCW.animate()
                    .alpha(0f)
                    .rotationBy(3600)
                    .setDuration(2000);

            LJS.animate()
                    .alpha(1f)
                    .rotationBy(3600)
                    .setDuration(2000);
            ch++;
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LJS = (ImageView) findViewById(R.id.LJS);
        JCW = (ImageView) findViewById(R.id.JCW);

    }
}
